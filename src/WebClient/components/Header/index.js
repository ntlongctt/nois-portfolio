/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useLayoutEffect } from "react";
import { ContainerHeader, ContainerFluid, Header, NavBar } from "./Header";

import { Navbar } from "react-bootstrap";
import logo from "../../assets/image/logo.png";

const HeaderComponent = () => {
  const [isScroll, setIsScroll] = useState(false);
  const [hasFirst, setHasFirst] = useState(true);

  useLayoutEffect(() => {
    window.addEventListener("scroll", () => {
      const _scrollY = window.scrollY;

      if (_scrollY > 400) {
        setIsScroll(true);
        return;
      }

      if (_scrollY > 200) setHasFirst(false);
      else setHasFirst(true);
      setIsScroll(false);
    });
  }, []);

  return (
    <ContainerHeader>
      <Header
        active={isScroll}
        className={hasFirst ? "firstHeader" : "secondHeader"}
      >
        <ContainerFluid className="container-fluid">
          <NavBar>
            <Navbar.Brand href="#home">
              <img src={logo} alt="" />
            </Navbar.Brand>
          </NavBar>
        </ContainerFluid>
      </Header>
    </ContainerHeader>
  );
};
export default HeaderComponent;
