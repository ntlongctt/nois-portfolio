/* eslint-disable import/no-anonymous-default-export */
import styled from "styled-components";
import { Navbar } from "react-bootstrap";

const ContainerHeader = styled.div`
  width: 100%;
  height: 100%;

  .secondHeader {
    position: fixed;
    background: black;
    left: 0;
    top: -400px;
    z-index: 10;
    opacity: 0;
    visibility: hidden;
    transition: all 1s;
    width: 100%;

    padding-right: 9px;
    padding-left: 2px;
  }

  .firstHeader {
    background: black;
    width: 100%;

    display: flex;
    justify-content: center;
    align-items: center;
    transition: all 0.1s;

    @media only screen and (min-width: 320px) {
      display: block;
      background: #fff;
      .show-menu {
        width: 250px;
        flex-direction: column;
        .btn__close {
          display: block;
        }
      }
    }

    @media only screen and (min-width: 576px) {
      display: block;
      background: #fff;
      .show-menu {
        width: 250px;
        flex-direction: column;
        .btn__close {
          display: block;
        }
      }
    }

    @media only screen and (min-width: 768px) {
      display: block;
      background: #fff;
      .show-menu {
        width: 300px;
        flex-direction: column;
        .btn__close {
          display: block;
        }
      }
    }

    @media only screen and (min-width: 992px) {
      display: block;
      background: black;
      flex-direction: row;
    }

    @media only screen and (min-width: 1024px) {
      display: block;
      background: black;
      flex-direction: row;
    }
  }
`;

const ContainerFluid = styled.div`
  padding-right: 10px;
  padding-left: 10px;
`;

const Header = styled.div`
  ${({ active }) =>
    active &&
    `
    opacity: 1 !important;
    top: 0 !important;
    visibility: visible !important;
    background: #333 !important;
    
  `}

  @media only screen and (min-width: 320px) {
    display: block;
    .show-menu {
      width: 250px;
      flex-direction: column;
      .btn__close {
        display: block;
      }
    }
  }

  @media only screen and (min-width: 576px) {
    display: block;
    .show-menu {
      width: 250px;
      flex-direction: column;
      .btn__close {
        display: block;
      }
    }
  }

  @media only screen and (min-width: 768px) {
    display: block;
    .show-menu {
      width: 300px;
      flex-direction: column;
      .btn__close {
        display: block;
      }
    }
  }

  @media only screen and (min-width: 992px) {
    display: block;
  }

  @media only screen and (min-width: 1024px) {
    display: block;
  }
`;

const NavBar = styled(Navbar)`
  margin-left: 170px;
  margin-right: 170px;
  height: 100px
  padding: 5px 0px;
  img{
    width: 130px;
  }
  @media only screen and (min-width: 320px) {
   
    margin-left: 0px;
    margin-right: 0px;
    padding: 0.3rem 0rem;
  }

  @media only screen and (min-width: 576px) {
   
    margin-left: 0px;
    margin-right: 0px;
    padding: 0.3rem 0rem;
  }

  @media only screen and (min-width: 768px) {
    margin-left: 0px;
    margin-right: 0px;
    padding: 0.3rem 0rem;
  }
  @media only screen and (min-width: 992px) {
    margin-left: 15px;
    margin-right: 15px;
    height: 100px
  }
  @media only screen and (min-width: 1024px) {
    margin-left: 15px;
    margin-right: 15px;
    height: 100px
  }
  @media only screen and (min-width: 1200px) {
    margin-left: 170px;
    margin-right: 170px;
    height: 100px
  }
  @media only screen and (min-width: 1400px) {
    margin-left: 387px;
    margin-right: 370px;
    height: 100px
  }
`;

export { ContainerFluid, Header, NavBar, ContainerHeader };
