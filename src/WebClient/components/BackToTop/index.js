import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { ArrowUp } from "react-feather";

const ScrollTop = styled.div`
  .back__to__top {
    width: 45px;
    height: 45px;
    line-height: 45px;

    text-align: center;
    position: fixed;
    z-index: 10;

    right: 15px;
    bottom: 15px;
    transition: all 0.2s;

    background-color: #fa5c5d;
    border-radius: 3px;
    cursor: pointer;

    animation: fadeIn 700ms ease-in-out 0.2s both;   
    @keyframes fadeIn {
        from {
          opacity: 0;
        }
        to {
          opacity: 1;
        }
      }

    &:hover {
      background-color: #f6483b;
    }

    @media only screen and (min-width: 320px) {
      width: 35px;
      height: 35px;
      line-height: 35px;
      margin-right: 7px;
    }

    @media only screen and (min-width: 576px) {
      width: 35px;
      height: 35px;
      line-height: 35px;
    }

    @media only screen and (min-width: 768px) {
      width: 35px;
      height: 35px;
      line-height: 35px;
    }
    @media only screen and (min-width: 992px) {
      width: 45px;
      height: 45px;
      line-height: 45px;
    }
    @media only screen and (min-width: 1024px) {
      width: 45px;
      height: 45px;
      line-height: 45px;
    }
  }
`;

const BackToTop = () => {
  const [isVisible, setIsVisible] = useState(false);

  const handleScrollTop = () => {
    if (window.pageYOffset > 50) {
      setIsVisible(true);
    } else {
      setIsVisible(false);
    }
  };

  const scrollToTop = () => {
    window.scrollTo({
      behavior: "smooth",
      top: 0,
    });
  };


  useEffect(() => {
    window.addEventListener("scroll", handleScrollTop);
  }, []);


  return (
    <ScrollTop>
      {isVisible && (
        <div className="back__to__top" onClick={scrollToTop} >
          <ArrowUp color="white" size="25" />
        </div>
      )}
    </ScrollTop>
  );
};
export default BackToTop;
