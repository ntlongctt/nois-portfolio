import React from "react";
import logo from "../../assets/image/logo.png";
import { Row, Col } from "react-bootstrap";
import styled from "styled-components";



const FooterContainer = styled.div`
  width: 100%;
  min-height: 100px;
  background: #222;
  padding: 20px 0 10px;
  .footer__logo{
    img{
      width: 130px;
      margin-top: 6px;
    }
    text-align: left;
    @media only screen and (min-width: 320px) {
      text-align: center;
    }
    @media only screen and (min-width: 768px) {
      text-align: left;
    }
    @media only screen and (min-width: 1200px) {
      text-align: left;
    }
   
  }
 
  .footer__content{
    p {
       color: #ddd;
        text-align: right;
        margin-top: 15px;
        font-size: 14px;
        @media only screen and (min-width: 320px) {
          text-align: center;
        }
        @media only screen and (min-width: 768px) {
          text-align: right;
        }
        @media only screen and (min-width: 1200px) {
          text-align: right;
          
        }
    }
  }
  
  
`;

const SectionFooter = styled.div`
    padding-right: 0px; 
    padding-left: 0px;
`;

const Footer = () => {
  return (
    <FooterContainer>
      <SectionFooter className="container">
        <Row>
          <Col sm={6} md={6} xs={12} className="footer__logo">
            <img src={logo} alt="logo" />
          </Col>
          <Col sm={6} md={6} xs={12} className="footer__content">
            <p>Copyright © 2021 by NOIS</p>
           
          </Col>
        </Row>
      </SectionFooter>
      
    </FooterContainer>
  );
};
export default Footer;
