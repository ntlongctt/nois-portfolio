import React from "react";
import { Carousel } from "react-bootstrap";
import styled from "styled-components";

const Section = styled.div`
  width: 100%;
  height: 100%;
  margin-top: 20px;
  .carousel__container{
    img{
      height: 500px;
    }
    .carousel-indicators{
      display:none;
    }
  }
`;  

const CarouselProject = (props) => {
  const { listSlides } = props;

  const renderCarousel = () => {
    return listSlides.map((item, index) => {
      return (
        <Carousel.Item key={index}>
          <img className="w-100" src={item.img} alt="First slide" />
        </Carousel.Item>
      );
    });
  };

  return (
    <Section>
      <div className="carousel__container">
        <Carousel>{renderCarousel()}</Carousel>
      </div>
    </Section>
  );
};

export default CarouselProject;
