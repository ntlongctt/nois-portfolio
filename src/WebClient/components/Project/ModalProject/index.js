import React from "react";
import { Modal } from "react-bootstrap";
import styled from "styled-components";
import CarouselProject from "../CarouselProject";
import { ChevronRight } from "react-feather";

const SectionModal = styled.div`
  z-index: 999;
`;

const ModalContent = styled(Modal)`
  .modal-content {
    background-color: #4b4c4d !important;
  }
`;

const ModalBody = styled(Modal.Body)`
  color: #fff;
  .modal__content {
    width: 100%;
    display: flex;
    .modal__content__detail {
      width: 65%;
      padding-right: 20px;
    }
    .modal__content__infor {
      width: 35%;
    }
    @media only screen and (min-width: 320px) {
      h3 {
        font-size: 18px;
      }
      .modal__content__detail {
        font-size: 13px;
      }
      .modal__content__infor {
        font-size: 13px;
      }
    }

    @media only screen and (min-width: 576px) {
      h3 {
        font-size: 18px;
      }
      .modal__content__detail {
        font-size: 13px;
      }
      .modal__content__infor {
        font-size: 13px;
      }
    }

    @media only screen and (min-width: 768px) {
      h3 {
        font-size: 24px;
      }
      .modal__content__detail {
        font-size: 16px;
      }
      .modal__content__infor {
        font-size: 16px;
      }
    }
    @media only screen and (min-width: 992px) {
      h3 {
        font-size: 24px;
      }
      .modal__content__detail {
        font-size: 16px;
      }
      .modal__content__infor {
        font-size: 16px;
      }
    }
    @media only screen and (min-width: 1024px) {
      h3 {
        font-size: 24px;
      }
      .modal__content__detail {
        font-size: 16px;
      }
      .modal__content__infor {
        font-size: 16px;
      }
    }
  }
`;

const CarouselDiv = styled.div`
  padding-left: 20px;
  padding-right: 20px;
  padding-bottom: 20px;
`;

const ModalHeader = styled(Modal.Header)`
  border-bottom: 1px solid #545b62;
  h4 {
    color: #fa5c5d;
    text-transform: uppercase;
  }
`;

const arrSildes = [
  {
    id: 1,
    img:
      "http://themepresss.com/tf/html/telusa/telusa/assets/images/gallery/img-4.jpg",
  },
  {
    id: 2,
    img:
      "http://themepresss.com/tf/html/telusa/telusa/assets/images/gallery/img-3.jpg",
  },
  {
    id: 3,
    img:
      "http://themepresss.com/tf/html/telusa/telusa/assets/images/gallery/img-2.jpg",
  },
  {
    id: 4,
    img:
      "http://themepresss.com/tf/html/telusa/telusa/assets/images/gallery/img-1.jpg",
  },
  {
    id: 5,
    img:
      "http://themepresss.com/tf/html/telusa/telusa/assets/images/gallery/img-5.jpg",
  },
  {
    id: 6,
    img:
      "http://themepresss.com/tf/html/telusa/telusa/assets/images/gallery/img-6.jpg",
  },
];

const ModalProject = (props) => {
  return (
    <SectionModal>
      <ModalContent
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <ModalHeader closeButton>
          <h4>Project One</h4>
        </ModalHeader>
        <CarouselDiv>
          <CarouselProject listSlides={arrSildes} />
        </CarouselDiv>
        <ModalBody>
          <div className="modal__content">
            <div className="modal__content__detail">
              <h3>Project Details</h3>
              <p>
                Cras mattis consectetur purus sit amet fermentum. Cras justo
                odio, dapibus ac facilisis in, egestas eget quam. Morbi leo
                risus, porta ac consectetur ac, vestibulum at eros.
              </p>
              <p>
                Cras mattis consectetur purus sit amet fermentum. Cras justo
                odio, dapibus ac facilisis in, egestas eget quam. Morbi leo
                risus, porta ac consectetur ac, vestibulum at eros.
              </p>
            </div>
            <div className="modal__content__infor">
              <h3>Information</h3>
              <div>
                <div>
                  <ChevronRight size={20} />
                  <span>HTML/CSS</span>
                </div>
                <div>
                  <ChevronRight size={20} />
                  <span>Logo Design</span>
                </div>
                <div>
                  <ChevronRight size={20} />
                  <span>User Interface Design</span>
                </div>
              </div>
            </div>
          </div>
        </ModalBody>
      </ModalContent>
    </SectionModal>
  );
};

export default ModalProject;
