/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from "react";
import styled from "styled-components";
import CategoryLink from "./CategoryLink";
import ListProject from "./ListProject";

const Section = styled.div`
  background: #202020;
  padding-bottom: 150px;
  padding-top: 100px;
`;

const DivTitle = styled.div`
  margin-bottom: 20px;
  text-align: center;
`;

const Title = styled.h2`
  font-size: 29px;
  font-weight: 700;
  color: #fff;

  padding-bottom: 16px;
  position: relative;
  display: inline-block;
  &::before {
    content: "";
    position: absolute;
    left: 37%;
    bottom: 0;
    height: 5px;
    width: 55px;
    background-color: #fa5c5d;
  }
  &::after {
    content: "";
    position: absolute;
    left: 0;
    bottom: 2px;
    height: 1px;
    width: 100%;
    background-color: #fa5c5d;
  }
  @media only screen and (min-width: 320px) {
    font-size: 25px;
  }

  @media only screen and (min-width: 576px) {
    font-size: 25px;
  }

  @media only screen and (min-width: 768px) {
    font-size: 29px;
  }
  @media only screen and (min-width: 992px) {
    font-size: 29px;
  }
  @media only screen and (min-width: 1024px) {
    font-size: 29px;
  }
`;

const SectionTwo = styled.div`
  margin-top: 30px;
  .show--img {
    opacity: 1;
    transform: scale(1);
    transition-property: opacity, transform;
    transition-duration: 0.4s;
  }
`;



const arrCategoryLink = [
  { id: 1, name: "All" },
  { id: 2, name: "Web Design" },
  { id: 3, name: "Print Design" },
  { id: 4, name: "Web Application" },
  { id: 5, name: "Photography" },
];

const arrListProject = [
  {
    id: 1,
    name: "Project One",
    category: [1, 3, 4, 5],
    img:
      "http://themepresss.com/tf/html/telusa/telusa/assets/images/gallery/img-1.jpg",
  },
  {
    id: 2,
    name: "Project Two",
    category: [1, 2, 3, 4],
    img:
      "http://themepresss.com/tf/html/telusa/telusa/assets/images/gallery/img-2.jpg",
  },
  {
    id: 3,
    name: "Project Three",
    category: [1, 3, 5],
    img:
      "http://themepresss.com/tf/html/telusa/telusa/assets/images/gallery/img-3.jpg",
  },
  {
    id: 4,
    name: "Project Four",
    category: [1, 2, 4],
    img:
      "http://themepresss.com/tf/html/telusa/telusa/assets/images/gallery/img-4.jpg",
  },
  {
    id: 5,
    name: "Project Five",
    category: [1, 3, 5],
    img:
      "http://themepresss.com/tf/html/telusa/telusa/assets/images/gallery/img-5.jpg",
  },
  {
    id: 6,
    name: "Project Six",
    category: [1, 2, 3, 5, 4],
    img:
      "http://themepresss.com/tf/html/telusa/telusa/assets/images/gallery/img-6.jpg",
  },
];

const Project = () => {
  const [projectToDisplay, setProjectToDisplay] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState(1);

  useEffect(() => {
    const filteredProject = arrListProject.filter((p) =>
      p.category.includes(selectedCategory)
    );
    setProjectToDisplay(filteredProject);
  }, [selectedCategory]);

  const handleChangeCategory = (id) => {
    setSelectedCategory(id);
  };

  return (
    <div>
      <Section>
        <DivTitle>
          <Title>Work we have Done</Title>
        </DivTitle>
        <SectionTwo className="container">
          <CategoryLink
            listCategory={arrCategoryLink}
            onSelectCategory={handleChangeCategory}
          />
          <ListProject listProject={projectToDisplay} />
        </SectionTwo>
       
      </Section>
    </div>
  );
};

export default Project;
