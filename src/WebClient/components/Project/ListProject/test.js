import React, { Component } from "react";

import shuffle from "lodash/shuffle";
import FlipMove from "react-flip-move";

const articles = [
  {
    id: "a",
    timestamp: 811396800000,
    name: "Netscape 2.0 ships, introducing Javascript",
  },
  {
    id: "b",
    timestamp: 1108702800000,
    name: "Jesse James Garrett releases AJAX spec",
  },
  { id: "c", timestamp: 1156564800000, name: "jQuery 1.0 released" },
  { id: "d", timestamp: 1256443200000, name: "First underscore.js commit" },
  { id: "e", timestamp: 1286942400000, name: "Backbone.js becomes a thing" },

  { id: "f", timestamp: 1331697600000, name: "Angular 1.0 released" },
  {
    id: "g",
    timestamp: 1369800000000,
    name: "React is open-sourced; developers rejoice",
  },
];
class ListItem extends Component {
  render() {
    return (
      <li>
        <h3>{this.props.name}</h3>
      </li>
    );
  }
}


class Shuffle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      articles,
    };

    this.sortShuffle = this.sortShuffle.bind(this);
  }

  sortShuffle() {
    this.setState({
      articles: shuffle(this.state.articles),
    });
  }

  
  renderArticles() {
    return this.state.articles.map((article, i) => {
      return <ListItem key={article.id} index={i} {...article} />;
    });
  }

  render() {
    return (
      <div id="shuffle" className={this.state.view}>
        <header>
          <div className="abs-left">
            <button onClick={this.sortShuffle}>aaaaa</button>
          </div>
        </header>
        <div>
          <FlipMove staggerDurationBy="30" duration={500}>
            {this.renderArticles()}
          </FlipMove>
        </div>
      </div>
    );
  }
}

export default Shuffle;
