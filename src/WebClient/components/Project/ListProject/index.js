import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Row, Col } from "react-bootstrap";

import { Eye } from "react-feather";
import ModalProject from "../ModalProject";
import WOW from 'wowjs';


const Column = styled(Col)`
  padding-right: 0px;
  padding-left: 0px;
  padding: 0 7.5px 15px;
  transition: all 0.5s;
  transform: scale(1);

  @media only screen and (min-width: 320px) {
    padding-right: 15px;
    padding-left: 15px;
  }

  @media only screen and (min-width: 576px) {
    padding-right: 15px;
    padding-left: 15px;
  }

  @media only screen and (min-width: 768px) {
    padding: 0 7.5px 15px;
  }
  @media only screen and (min-width: 992px) {
    padding: 0 7.5px 15px;
  }
  @media only screen and (min-width: 1024px) {
    padding: 0 7.5px 15px;
  }
`;
const ProjectContent = styled.div`
  .project__img {
    img {
      width: 100%;
      &::before {
        content: "";
        transition: all 0.2s;
        transform: scale(1.5);
      }
    }

    &::before {
      position: absolute;
      content: "";
      left: 5%;
      top: 3%;
      width: 90%;
      height: 90%;
      background: rgba(0, 0, 0, 0.5);
      transition: all 0.3s;
      z-index: 2;
      visibility: hidden;
      opacity: 0;
      transform: scale(0);
      
    }
    &:hover:before {
      opacity: 1;
      visibility: visible;
      transform: scale(1);
    }
    .icon {
      position: absolute;
      top: 43%;
      left: 0;
      bottom: auto;
      width: 100%;
      height: 100%;
      text-align: center;
      z-index: 999;
      transform: scale(0);
      cursor: pointer;
      }
    }
    &:hover .icon {
      transform: scale(1);
    }
  }
`;

const ListProject = (props) => {
  const { listProject } = props;

  const [modalShow, setModalShow] = useState(false);

  useEffect(()=>{
    new WOW.WOW({
      live: false
  }).init();
  })

  const renderProjectList = () => {
    return listProject.map((item, index) => {
      return (
        <Column xs={12} md={4} key={index} className="wow zoomIn">
          <ProjectContent>
            <div className="project__img" onClick={() => setModalShow(true)}>
              <img src={item.img} alt={item.name} />
              <div className="icon">
                <Eye color="white" size={30} />
              </div>
            </div>
          </ProjectContent>
        </Column>
      );
    });
  };

  return (
    <>
      <Row>{renderProjectList()}</Row>
      <ModalProject show={modalShow} onHide={() => setModalShow(false)} />
    </>
  );
};

export default ListProject;
