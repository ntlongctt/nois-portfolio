/* eslint-disable array-callback-return */

import React, { useState } from "react";
import styled from 'styled-components'

const SectionLink = styled.div`
  margin-bottom: 40px;
  text-align: center;
  margin-left: 30px;
`

const WrapUl = styled.ul`
  padding-left: 0px;
  list-style-type: none;
  text-align: center;
  display: inline;
 
  .gallery__li {
    color: #fa5c5d !important;
  }
`;

const WrapLi = styled.li`
  display: inline-block;
  margin-left: 20px;
  padding-top: 20px;
  a {
    color: #b0b0b0 !important;
    font-weight: 400;
    text-decoration: none;
    cursor: pointer;
    font-size: 17px;
  }
  a:active {
    color: #fa5c5d !important;
  }

  @media only screen and (min-width: 320px) {
    margin-left: 0;
    margin-right: 20px;
    margin-bottom: 10px;
    padding-top: 0px;
  
  }

  @media only screen and (min-width: 576px) {
    margin-left: 0;
    margin-right: 27px;
    margin-bottom: 10px;
   
  }

 
`;

const CategoryLink = (props) => {

  const { listCategory, onSelectCategory } = props
  const [isActive, setIsActive]= useState(1)


  const handleCategoryLink = (id)=>{
      setIsActive(id);
      onSelectCategory(id);
  }

  
  const renderCategoryLink = ()=>{
    return listCategory.map((item, index)=>{
      return(
        <WrapLi key={index}>
          <a
            onClick={() => handleCategoryLink(item.id)}
            className={`${isActive === item.id && 'gallery__li'}`}
            href="#project"
          >
            {item.name}
          </a>
        </WrapLi>
      )
    })
  }

  return (
    <>
      <SectionLink>
        <WrapUl>
            {renderCategoryLink()}
        </WrapUl>
      </SectionLink>
     
    </>
  );
};

export default CategoryLink;
