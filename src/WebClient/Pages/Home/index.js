import React  from "react";
import BackToTop from "../../components/BackToTop";
import Footer from "../../components/Footer";
import HeaderComponent from "../../components/Header";
import Project from "../../components/Project";



const Home = () => {
  return (
    <>
      <HeaderComponent />    
      <Project />
      <Footer />
      <BackToTop />
    </>
  );
};

export default Home;
