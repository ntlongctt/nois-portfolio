import './App.css';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import Home from "../src/WebClient/Pages/Home"
import HomeAdmin from "../src/WebAdmin/pages/HomeAdmin"
import ProtectedRoute from './WebAdmin/pages/ProtectedRoute';
import SignIn from "../src/WebAdmin/pages/SignIn"
import SignUp from "../src/WebAdmin/pages/SignUp"


function App() {
  return (
    <BrowserRouter>
        <Switch>
          <Route exact path='/' component={Home} />
          <ProtectedRoute exact path='/admin' component={HomeAdmin} />
          <ProtectedRoute exact path='/admin/signin' component={SignIn} />
          <ProtectedRoute exact path='/admin/signup' component={SignUp} />
        </Switch>
    </BrowserRouter>
  );
}

export default App;
