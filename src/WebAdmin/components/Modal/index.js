import React from "react";
import { Modal, Button } from "react-bootstrap";
import styled from "styled-components";

const Section = styled(Modal)`
  .modal-body {
    text-align: center;
    padding: 0; 
    padding-top: 32px;
    
}
  }
  .modal-footer {
    border-top: none;
    justify-content: center;
    .btn__close {
      border: none;
      background-color: #fa5c5d;
    }
  }
`;

const ModalNotification = (props) => {
  return (
    <Section
      {...props}
      size="sm"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Body>
        <p>Create account success!</p>
      </Modal.Body>

      <Modal.Footer>
        <Button className="btn__close" onClick={props.onHide}>
          OK
        </Button>
      </Modal.Footer>
    </Section>
  );
};

export default ModalNotification;
