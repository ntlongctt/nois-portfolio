import firebase from "firebase/app";
import "firebase/auth";
import env from "../../env"

debugger


const firebaseConfig = {
    apiKey: env.REACT_APP_API_KEY,
    authDomain: env.REACT_APP_AUTH_DOMAIN,
    projectId: env.REACT_APP_PROJECT_ID,
    
    storageBucket: env.REACT_APP_STORAGE_BUCKET,
    messagingSenderId: env.REACT_APP_MESSAGESENDER_ID,
    appId: env.REACT_APP_APP_ID
  };
  // Initialize Firebase
   firebase.initializeApp(firebaseConfig);

 export const auth = firebase.auth();