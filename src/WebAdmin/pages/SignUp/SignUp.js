import styled from "styled-components";
import { Form } from "react-bootstrap";

const SectionSignUp = styled.div`
  width: 100%;
  min-height: 100vh;
  background-image: linear-gradient(
    to right,
    rgb(0 123 255 / 79%),
    rgb(0 123 255 / 10%)
  );
`;

const ContainerSignUp = styled.div`
  width: 100%;
  min-height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;

  .signup__container {
    width: 300px;
    height: 80%;
    background-color: black;
    color: #fff;
    box-shadow: 0px 2px 2px 2px #cccccc;

    .signup__content {
      padding: 35px;

      .signup__content__title {
        padding-top: 40px;
        span {
          font-size: 30px;
          font-weight: 700;
        }
        p {
          margin-top: -10px;
        }
      }
    }
    .signup__account {
      margin-top: 70px;

      .icon {
        display: inline-block;
        position: relative;
        width: 35px;
        height: 55px;
        border: 1px solid black;

        background: black;
        top: 32px;
        left: 231px;
        border-radius: 25px;

        .icon__down {
          position: relative;
          top: 22px;
          left: 4px;
        }
      }
      button {
        border: none;
        width: 100%;
        background: #fff;
        text-align: center;
        padding: 20px;
        a {
          text-decoration: none;
          color: black;
          font-size: 16px;
        }
      }
    }
  }
`;

const FormSignUp = styled(Form)`
  input {
    border: none;
    background: black;
    border-bottom: 1px solid #a29d9d;
    width: 100%;
    color: #fff;
    &:focus {
      outline: none;
    }
    &:-internal-autofill-selected {
      background-color: black !important;
    }
    &:-webkit-autofill,
    &:-webkit-autofill:hover, 
    &:-webkit-autofill:focus, 
    &:-webkit-autofill:active  {
      -webkit-box-shadow: 0 0 0 30px black inset;
      -webkit-text-fill-color: #fff !important;
    }
  }
  .invalid__feedback{
    font-size: 14px;
    color: red;
  }
  .form__name {
    margin-top: 70px;
  }
  .form__email {
    margin-top: 30px;
  }
  .form__password {
    margin-top: 30px;
  }

  button {
    border: none;
    width: 100%;
    text-transform: uppercase;
    margin-top: 40px;
    background-color: #fff;
    color: black;
    border-radius: 5px;
    padding: 8px 0px;
  }
`;

export { SectionSignUp, ContainerSignUp, FormSignUp };
