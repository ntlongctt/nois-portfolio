/* eslint-disable default-case */
import React, { useState, useEffect } from "react";
import { ChevronDown } from "react-feather";
import { Link } from "react-router-dom";

import { auth } from "../../FirebaseService/index";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";

import { SectionSignUp, ContainerSignUp, FormSignUp } from "./SignUp.js";
import ModalNotification from "../../components/Modal/index.js";

const SignUp = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("");

  const [passwordError, setPasswordError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [user, setUser] = useState("");
  const [userAccount, setUserAccount] = useState(false);
  const [modalShow, setModalShow] = useState(false);

  const authenticateUser = (email, password, isLogin) => {
    try {

      const user = isLogin
        ? auth.signInWithEmailAndPassword(email, password)
        : auth.createUserWithEmailAndPassword(email, password);
      setUserAccount(true);
      console.log(user);

    } catch (err) {

      switch (err.code) {
        case "auth/user-not-found":
        case "auth/user-disable":
        case "auth/invalid-email":
        case "auth/email-already-in-use":
          setEmailError(err.message);
          break;
        case "auth/wrong-password":
          setPasswordError(err.message);
          break;
      }
    }
  };

  const onChangeHandler = (event) => {
    const { name, value } = event.currentTarget;
    if (name === "email") {
      setEmail(value);
    } else if (name === "password") {
      setPassword(value);
    } else if (name === "username") {
      setUsername(value);
    }
  };

  const authListener = () => {
    //user management
    auth.onAuthStateChanged((user) => {
      if (user) {
        setUser(user);
      } else {
        setUser("");
      }
    });
  };

  useEffect(() => {
    authListener();
  });

  const validationSchema = Yup.object().shape({
    username: Yup.string().required("Username is required"),
    email: Yup.string()
      .required(`${emailError}`)
      .matches(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/, "Email is not format"),

    password: Yup.string()
      .min(6, "Password must be at least 6 characters")
      .required(`${passwordError}`),
  });

  const { register, errors, handleSubmit } = useForm({
    resolver: yupResolver(validationSchema),
  });

  return (
    <SectionSignUp>
      <ContainerSignUp>
        <div className="signup__container">
          <div className="signup__content">
            <div className="signup__content__title">
              <span>Welcome</span>
              <p>glad to see you!</p>
            </div>
            <FormSignUp
              onSubmit={handleSubmit(() => {
                authenticateUser(email, password, false);
              })}
            >
              <input
                placeholder="Name"
                className="form__name"
                name="username"
                value={username}
                ref={register}
                onChange={(event) => {
                  onChangeHandler(event);
                }}
              />
              <span className="invalid__feedback">
                {errors.username?.message}
              </span>

              <input
                placeholder="Email"
                className="form__email"
                name="email"
                ref={register}
                value={email}
                onChange={(event) => {
                  onChangeHandler(event);
                }}
              />
              <span className="invalid__feedback">{errors.email?.message}</span>

              <input
                placeholder="Password"
                className="form__password"
                name="password"
                ref={register}
                value={password}
                onChange={(event) => {
                  onChangeHandler(event);
                }}
              />
              <span className="invalid__feedback">
                {errors.password?.message}
              </span>

              <button type="submit" >create account</button>
              <span className="invalid__feedback">
                {userAccount === true && "Create account success!"}
              </span>
            </FormSignUp>
          </div>

          <div className="signup__account">
            <div className="icon">
              <ChevronDown color="white" size={25} className="icon__down" />
            </div>
            <button>
              <Link to="/admin/signin">Login</Link>
            </button>
          </div>
          <ModalNotification  show={modalShow}
        onHide={() => setModalShow(false)} />
        </div>
      </ContainerSignUp>
    </SectionSignUp>
  );
};
export default SignUp;
