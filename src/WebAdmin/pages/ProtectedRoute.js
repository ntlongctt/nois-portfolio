import React from "react";
import {Route, Redirect} from "react-router-dom"; 

const ProtectedRoute = ({children, ...rest})=>{
    return(
        <Route {...rest}
            render={props =>{
                <Redirect to={{ pathname: '/', state: {from : props.location} }} />
            }}
        
        />
    );
}

export default ProtectedRoute;