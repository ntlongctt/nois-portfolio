import styled from "styled-components";
import { Form } from "react-bootstrap";

const SectionSignIn = styled.div`

width: 100%;
min-height: 100vh;
background-image: linear-gradient(to right, rgb(0 123 255 / 79%) , rgb(0 123 255 / 10%));
`;

const ContainerSignIn = styled.div`
width: 100%;
min-height: 100vh;
display: flex;
justify-content: center;
align-items: center;
position: relative;

.signin__container {
  width: 300px;
  height: 80%;
  background-color: #fff;
  box-shadow: 0px 2px 2px 2px #cccccc;

  .signin__content {
    padding: 35px;

    .signin__content__title {
      padding-top: 40px;
      span {
        font-size: 30px;
        font-weight: 700;
      }
      p {
        margin-top: -10px;
      }
    }
  }
  .signin__content__account {
      margin-top: 146px;
      .icon{
          display: inline-block;
          position: relative;
          width: 35px;
          height: 55px;

          border: 1px solid black;
          background: black;
          top: 20px;
          left: 232px;
          border-radius: 25px;

          .icon__up{
            position: relative;
            left: 4px;
            top: 3px;
          }
      }
      button{
        border: none;
        width: 100%;
        background: black;
        text-align: center;
        padding: 20px;
        a {
          text-decoration: none;
          color: #fff;
        }
      }
      
    }
}
`;

const FormSignIn = styled(Form)`
input {
  border: none;
  border-bottom: 1px solid #a29d9d;
  width: 100%;
  &:focus{
      outline: none;
  }
}
.form__email {
  padding-top: 70px;
}
.form__password {
  padding-top: 30px;
}
button {
  border: none;
  width: 100%;
  text-transform: uppercase;
  margin-top: 40px;
  background-color: black;
  color: #fff;
  border-radius: 5px;
  padding: 8px 0px;
 
}
`;

export {
    SectionSignIn,
    ContainerSignIn,
    FormSignIn
}