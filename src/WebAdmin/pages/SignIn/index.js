import React from "react";

import { ChevronUp } from "react-feather";
import { Link } from "react-router-dom";
import { SectionSignIn, ContainerSignIn, FormSignIn } from "./SignIn.js";

const SignIn = () => {
  return (
    <SectionSignIn>
      <ContainerSignIn>
        <div className="signin__container">
          <div className="signin__content">
            <div className="signin__content__title">
              <span>Welcome</span>
              <p>glad to see you!</p>
            </div>
            <FormSignIn>
              <input placeholder="Email" className="form__email" name="email" />
              <input
                placeholder="Password"
                className="form__password"
                name="password"
              />
              <button>login</button>
            </FormSignIn>
          </div>

          <div className="signin__content__account">
            <div className="icon">
              <ChevronUp color="white" size={25} className="icon__up" />
            </div>
            <button>
              <Link to="/admin/signup">Create account</Link>
            </button>
          </div>
        </div>
      </ContainerSignIn>
    </SectionSignIn>
  );
};
export default SignIn;
