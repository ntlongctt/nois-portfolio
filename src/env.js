import { cleanEnv, str } from "envalid";
export default cleanEnv(process.env, {
  REACT_APP_API_KEY: str(),
  REACT_APP_AUTH_DOMAIN: str(),
  REACT_APP_PROJECT_ID: str(),

  REACT_APP_STORAGE_BUCKET: str(),
  REACT_APP_MESSAGESENDER_ID: str(),
  REACT_APP_APP_ID: str(),
});
